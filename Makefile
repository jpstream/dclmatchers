SHELL=/bin/zsh

.PHONY: clean test dist all install

clean:
	find . -name __pycache__ -print0 | xargs -0 -r rm -rf
	find . -name '*.egg-info' -print0 | xargs -0 -r rm -rf
	rm -rf dist .pytest_cache
	rm -rf htmlcov .coverage

test:
	pytest --cov=src --cov-report html

dist:
	python3 setup.py sdist

install:
	pip3 install --user dist/dclmatchers-*.tar.gz

all: clean test dist
