# Copyright 2020 Steinar Knutsen
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
# European Commission - subsequent versions of the EUPL (the "Licence"); You may
# not use this work except in compliance with the Licence. You may obtain a copy
# of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# Licence for the specific language governing permissions and limitations under
# the Licence.

__version__ = "1.1.2"

from collections.abc import MutableMapping

def binarysearch(key, offset, max_offset, table):
    i  = offset + ((max_offset - offset) // 2)
    straw = table[i]
    if straw > key:
        if i == offset:
            return ~i
        return binarysearch(key, offset, i, table)
    elif straw < key:
        i += 1
        if i >= max_offset:
            return ~i
        return binarysearch(key, i, max_offset, table)
    else:
        return i

class AmbiguousKeyTable(ValueError):
    pass

class Entry:
    def __init__(self, name, value = None):
        if type(name) != type(""):
            raise TypeError(name)
        self.name = name
        self.value = value
    def __lt__(self, other):
        return self.name < other.name
    def __gt__(self, other):
        return self.name > other.name
 
class ListDclMatcher(MutableMapping):
    """
    A mapping data type which matches by shortest unique prefix.
    """
    def __init__(self, initdict = {}):
        """
        Create a mapper with initial mappings from the optional initdict.

        Do note this constructor is only exception safe if no initdict is given.
        """
        self.entries = []
        for key, value in list(initdict.items()):
            if not self.may_insert(key):
                raise AmbiguousKeyTable(key)
            self[key] = value
    def __getitem__(self, key):
        i = self._find(key)
        if i >= 0:
            return self.entries[i].value
        i = ~i
        if self._is_valid_key(key, i):
            return self.entries[i].value
        else:
            raise KeyError(key)
    def __setitem__(self, key, value):
        i = self._find(key)
        if i >= 0:
            self.entries[i] = Entry(self.entries[i].name, value)
            return
        i = ~i
        if self._is_valid_key(key, i):
            self.entries[i] = Entry(self.entries[i].name, value)
        else:
            if self._may_insert(key, i):
                self.entries.insert(i, Entry(key, value))
            else:
                raise AmbiguousKeyTable(key)
    def __delitem__(self, key):
        i = self._find(key)
        if i >= 0:
            del self.entries[i]
            return
        i = ~i
        if self._is_valid_key(key, i):
            del self.entries[i]
        else:
            raise KeyError(key)
    def __iter__(self):
        def entry_iterator(self):
            for entry in self.entries:
                yield entry.name
        return entry_iterator(self)
    def __len__(self):
        return len(self.entries)
    def full_key(self, key):
        """
        Get the complete key string for a possibly shortened key.
        """
        i = self._find(key)
        if i >= 0:
            return self.entries[i].name
        i = ~i
        if self._is_valid_key(key, i):
            return self.entries[i].name
        else:
            return None
    def may_insert(self, key):
        """
        Check whether a new key will neither overwrite existing entries nor
        lead to an ambiguous key table.
        """
        i = self._find(key)
        if i >= 0:
            return False
        i = ~i
        if self._is_valid_key(key, i):
            return False
        else:
            return self._may_insert(key, i)
    def shortest_key(self, key):
        """
        Return the length of the shortest unique prefix of the given key in
        the current key table.
        """
        if key not in self:
            return -1
        i = self._find(key)
        if i < 0:
            i = ~i
        prevshortest = self._previous_shortest(key, i)
        nextshortest = self._next_shortest(key, i)
        return max(nextshortest, prevshortest)
    def _find(self, key):
        if len(self.entries) == 0:
            return -1
        dummy = Entry(key)
        return binarysearch(dummy, 0, len(self.entries), self.entries)
    def _previous_shortest(self, key, index):
        if index == 0:
            return 1
        previous = self.entries[index - 1].name
        return self._shortest_unique_prefix(key, previous)
    def _next_shortest(self, key, index):
        if (index + 1) >= len(self.entries):
            return 1
        next = self.entries[index + 1].name
        return self._shortest_unique_prefix(key, next)
    def _shortest_unique_prefix(self, x, y):
        i = 0
        while x[i] == y[i]:
            i += 1
        return i + 1
    def _is_valid_key(self, key, index):
        if index >= len(self.entries):
            return False
        if not self.entries[index].name.startswith(key):
            return False
        next = index + 1
        if next >= len(self.entries):
            return True
        if self.entries[next].name.startswith(key):
            return False
        else:
            return True
    def _may_insert(self, key, index):
        previous = index - 1
        if previous >= 0 and key.startswith(self.entries[previous].name):
            return False
        next = index + 1
        # The following case is for when the key will lead to a ambiguous table.
        # _is_valid returns False both for nonexistent and ambiguous cases
        if next < len(self.entries) \
                and self.entries[next].name.startswith(key):
            return False
        return True
    def __repr__(self):
        return self.__str__()
    def __str__(self):
        return "ListDclMatcher%s" % str(list(self.items()))

# A proper trie in python very quickly ends up as nested, sorted lists or
# tuples, or as nested dictionaries. Both are rather expensive to traverse
# for small data sets, compared to a similarly populated dictionary.
# build_dictionary() is for building a dictionary where all unique prefixes from
# the given keys will resolve to the value in the given map.
# will_break_key_uniqueness() checks whether adding a single, given new key will
# lead to an ambiguous set of keys.

def find_shortest(name, names):
    """
    Utility function for build_dictionary(). Given a key and a set of keys, what
    is th shortest form of the key which will uniquely identify it in in a
    prefix string match.
    """
    least = 1
    versions = 2
    prefix = None
    while versions > 1 and least <= len(name):
        prefix = name[:least]
        versions = 0
        for candidate in names:
            if candidate.startswith(prefix):
                versions += 1
            if versions > 1:
                least += 1
                break
    if versions > 1:
        raise ValueError(name)
    else:
        return least

def build_dictionary(commands):
    """
    Populate a new dictionary with shortest unambiguous version of all given
    commands, and all other unique variants.
    """

    new_commands = {}
    command_names = list(commands.keys())
    # First loop and check the arguments once
    for command in command_names:
        if type(command) != type(""):
            raise TypeError(command)
        if len(command) < 1:
            raise ValueError(command)
    for command in command_names:
        least_length = find_shortest(command, command_names)
        action = commands[command]
        for i in range(least_length, len(command) + 1):
            new_commands[command[:i]] = action
    return new_commands

def will_break_key_uniqueness(needle, haystack):
    """
    If maintaining a set of names, check if a new name would lead to an
    ambigious key table for build_dictionary().
    """
    for straw in haystack:
        if straw.startswith(needle) or needle.startswith(straw):
            return True
    return False
